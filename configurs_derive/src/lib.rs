use proc_macro::TokenStream;
use quote::quote;
use syn;

#[cfg(feature="file_loader")]
fn impl_configuration_trait(name: &syn::Ident) -> proc_macro2::TokenStream {
    quote! {
        impl Configuration for #name where Self: serde::de::DeserializeOwned {
            fn load() -> #name {
                let mut config = #name::default();
                config.load_from_env();
                config
            }

            fn load_from_file<P: AsRef<std::path::Path>>(p: P)
                                                         -> Result<#name, ConfigurationError> {
                use std::io::prelude::*;

                let mut file = std::fs::File::open(p)
                    .map_err(|_| ConfigurationError("Could not open TOML config file."))?;

                let mut contents = String::new();
                file.read_to_string(&mut contents)
                    .map_err(|_| ConfigurationError("Could not read TOML config file contents."))?;

                let mut c : #name = toml::from_str(&contents)
                    .map_err(|_| ConfigurationError("Could not deserialize TOML config file."))?;

                c.load_from_env();

                Ok(c)
            }
        }
    }
}

#[cfg(not(feature="file_loader"))]
fn impl_configuration_trait(name: &syn::Ident) -> proc_macro2::TokenStream {
    quote! {
        impl Configuration for #name {
            fn load() -> #name {
                let mut config = #name::default();
                config.load_from_env();
                config
            }

            fn load_from_file<P: AsRef<std::path::Path>>(p: P) -> Result<#name, ConfigurationError> {
                Err(ConfigurationError("load_from_file requires the 'file_loader' feature"))
            }
        }
    }
}

fn impl_configurs(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let mut defaults = Vec::new();
    let mut envs = Vec::new();

    if let syn::Data::Struct(ref st) = ast.data {
        for fld in &st.fields {
            for attr in &fld.attrs {
                if let Ok(
                    syn::Meta::NameValue(
                        syn::MetaNameValue { path, lit: syn::Lit::Str(lit_str), .. }
                    )
                ) = attr.parse_meta() {
                    let identifier = fld.ident.as_ref().unwrap().to_owned();
                    let attribute_name = path.get_ident().unwrap().to_string();
                    let value = lit_str.to_owned();

                    if attribute_name == "default" {
                        defaults.push(quote! {
                            #identifier: #value.parse().expect(&format!(
                                "Could not parse default value for {}", stringify!(#identifier))
                            ),
                        });
                    } else if attribute_name == "env" {
                        envs.push(quote! {
                            if let Ok(val) = std::env::var(#value) {
                                self.#identifier = val.parse().expect(
                                    &format!("Could not parse '{}'", val)
                                );
                            }
                        });
                    }
                } else if let Ok(syn::Meta::Path(path)) = attr.parse_meta() {
                    let identifier = fld.ident.as_ref().unwrap().to_owned();
                    let attribute_name = path.get_ident().unwrap().to_string();
                    let ty = &fld.ty;

                    if attribute_name == "default" {
                        defaults.push(quote! {
                            #identifier: #ty::default(),
                        });

                        envs.push(quote! {
                            self.#identifier.load_from_env();
                        });
                    }
                }
            }
        }
    }

    let configuration_impl = impl_configuration_trait(&name);

    let gen = quote! {
        impl LoadableFromEnvironment for #name {
            fn load_from_env(&mut self) -> () {
                #(#envs)*
            }
        }

        impl std::default::Default for #name {
            fn default() -> Self {
                #name {
                    #(#defaults)*
                }
            }
        }

        #configuration_impl
    };

    gen.into()
}

#[proc_macro_derive(Configuration, attributes(env, default))]
pub fn configurs_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_configurs(&ast)
}

#[cfg(test)]
mod tests {}
