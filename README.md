# Configurs

Mildly opinionated configuration management for Rust apps. Supports loading
config values from environment variables, TOML files, and inline-annotated
default values.

## Usage

Add this to your `[dependencies]` section of `Cargo.toml`:

```toml
configurs = "0.1"
configurs_derive = { version = "0.1" }
```

...or, if you'd like to use the option to load configuration from a TOML file:

```toml
configurs = "0.1"
configurs_derive = { version = "0.1", features = ["file_loader"] }
```

Then, and annotate your struct:

```rust
use configurs_derive::Configuration;

#[derive(Configuration)]
struct Configuration {
    #[env="PORT"]
    #[default="8088"]
    port: i16,

    #[env="HOST"]
    #[default="127.0.0.1"]
    host: String,
}

fn main() {
    let config = Configuration::load();
}
```

Running the program with `HOST` or `PORT` environment variables will now
override the annotated defaults.

## Using the `file_loader` feature

Additional dependencies:

```toml
serde = "1"
serde_derive = "1"
toml = "0.5"
```

Usage:

```rust
use serde_derive::Deserialize;

#[derive(Configuration, Deserialize)]
#[serde(default)]
struct Configuration {
    #[env="PORT"]
    #[default="8088"]
    port: i16,

    #[env="HOST"]
    #[default="127.0.0.1"]
    host: String,
}

fn main() {
    let config = Configuration::load_from_file("./Config.toml");
}
```

The values will be loaded in the following order of precedence:

1. env variables
2. config file
3. annotated defaults
