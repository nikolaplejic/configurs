use std::path::Path;

#[derive(Debug)]
pub struct ConfigurationError(pub &'static str);

pub trait LoadableFromEnvironment {
    fn load_from_env(&mut self) -> ();
}

pub trait Configuration where Self: Default + LoadableFromEnvironment {
    fn load() -> Self;
    fn load_from_file<P: AsRef<Path>>(p: P) -> Result<Self, ConfigurationError>;
}

#[cfg(test)]
mod tests {
    use super::*;
    use configurs_derive::{Configuration};

    use serde_derive::Deserialize;

    use serial_test::serial;

    #[derive(Configuration, Debug, Deserialize)]
    struct ConfigSub {
        #[env="CI"]
        #[default="bla"]
        ci: String,
    }

    #[derive(Configuration, Debug, Deserialize)]
    #[serde(default)]
    struct Config {
        #[env="PORT"]
        #[default="8088"]
        port: i16,

        #[env="CONFIGURATION_ITEM"]
        #[default="configuration_item"]
        configuration_item: String,

        #[env="CONFIGURATION_ITEM_OTHER"]
        #[default="configuration_item_other"]
        configuration_item_other: String,

        #[default]
        sub: ConfigSub,
    }

    #[test]
    #[serial]
    fn test_default_impl() {
        let c = Config::load();
        assert!(c.configuration_item == "configuration_item");
        assert!(c.sub.ci == "bla");
    }

    #[test]
    #[serial]
    fn test_env_var_override() {
        std::env::set_var("CONFIGURATION_ITEM", "test");
        let c = Config::load();

        assert!(c.configuration_item == "test");
        assert!(c.sub.ci == "bla");

        std::env::remove_var("CONFIGURATION_ITEM");
    }

    #[test]
    #[serial]
    fn test_file_loader() {
        let c = Config::load_from_file("./fixtures/Config.toml");
        assert!(c.is_ok());

        let config = c.unwrap();
        assert!(config.configuration_item == "TOML file entry");
        assert!(config.configuration_item_other == "configuration_item_other");
    }

    #[test]
    #[serial]
    fn test_file_loader_with_env_var_override() {
        std::env::set_var("PORT", "9001");

        let c = Config::load_from_file("./fixtures/Config.toml");
        assert!(c.is_ok());

        let config = c.unwrap();
        assert!(config.configuration_item == "TOML file entry");
        assert!(config.configuration_item_other == "configuration_item_other");
        assert!(config.port == 9001);

        std::env::remove_var("PORT");
    }
}
